#!/usr/bin/env bash

# vim: set ft=sh

db_instance_name='placement-crm'

if [[ $(docker ps -f NAME=$db_instance_name -f STATUS=running -q --format '{{.Names}}') == $db_instance_name ]]; then
  :
elif [[ $(docker ps -f NAME=$db_instance_name -f STATUS=exited -q --format '{{.Names}}') == $db_instance_name ]]; then
  docker restart $db_instance_name
else
  docker run --network placement-crm --restart always -p 5432:5432 -e POSTGRES_PASSWORD=secret -h postgres --name $db_instance_name -d postgres:12-alpine postgres -c log_statement=all 
fi
