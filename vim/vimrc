filetype indent plugin on
syntax on
" set ignorecase
" set smartcase
set undofile
set ruler
set laststatus=2
set autoindent
set expandtab
set shiftwidth=2
set softtabstop=2
syntax enable
set backspace=2
set relativenumber
set number
set splitright
set tabstop=4

autocmd BufNewFile,BufRead *.go setlocal sw=8

highlight ExtraWhitespace ctermbg=red guibg=red
autocmd ColorScheme * highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/

let mapleader=","

let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_nr_show = 1

nmap <silent> <leader>t :TestNearest<CR>
nmap <silent> <leader>T :TestFile<CR>
nmap <silent> <leader>a :TestSuite<CR>
nmap <silent> <leader>l :TestLast<CR>
nmap <silent> <leader>g :TestVisit<CR>

call plug#begin('~/.vim/plugged')

" Plug 'https://github.com/junegunn/seoul256.vim.git'
Plug 'https://github.com/mileszs/ack.vim.git'
Plug 'https://github.com/junegunn/fzf.git', { 'do': { -> fzf#install() } }
Plug 'https://github.com/junegunn/fzf.vim.git'
Plug 'https://github.com/vim-airline/vim-airline.git'
Plug 'https://github.com/vim-airline/vim-airline-themes.git'
" Plug 'https://github.com/altercation/vim-colors-solarized.git'
Plug 'https://github.com/morhetz/gruvbox.git'
Plug 'https://github.com/tpope/vim-fugitive.git'
Plug 'https://github.com/fatih/vim-go.git'
Plug 'https://github.com/pangloss/vim-javascript.git'
Plug 'https://github.com/jgdavey/tslime.vim.git'
Plug 'https://github.com/vim-test/vim-test.git'
Plug 'https://github.com/tpope/vim-surround.git'
Plug 'https://github.com/posva/vim-vue.git'
Plug 'https://github.com/vim-pandoc/vim-pandoc.git'
Plug 'https://github.com/prettier/vim-prettier.git'
Plug 'https://github.com/jparise/vim-graphql.git'
Plug 'https://github.com/leafgarland/typescript-vim.git'
Plug 'https://github.com/mhinz/vim-mix-format.git'
Plug 'https://github.com/elixir-editors/vim-elixir.git'
Plug 'https://github.com/Chiel92/vim-autoformat.git'
Plug 'https://github.com/tpope/vim-projectionist.git'

call plug#end()

if !isdirectory($HOME."/.vim/undo")
    call mkdir($HOME."/.vim/undo", "p")
endif
set undodir=~/.vim/undo
set undofile

if !isdirectory($HOME."/.vim/swap")
    call mkdir($HOME."/.vim/swap", "p")
endif

set directory=$HOME/.vim/swap//


" colorscheme solarized
" colo seoul256
set background=dark
autocmd vimenter * colorscheme gruvbox

set timeoutlen=1000 ttimeoutlen=0

function! ToggleNumbersOn()
  set relativenumber!
  set number
endfunc

function! ToggleRelativeOn()
  set number!
  set relativenumber
  set number
endfunc

function! NumberToggle()
  if(&relativenumber == 1)
    set relativenumber!
    set number
  else
    set number!
    set relativenumber
    set number
 endif
endfunc

nnoremap <C-n> :call NumberToggle()<cr>
autocmd InsertEnter * :call ToggleNumbersOn()
autocmd InsertLeave * :call ToggleRelativeOn()

let g:ackprg = 'ag --vimgrep'
let g:pandoc#modules#disabled = ["folding"]

cnoreabbrev Ag Ack!

autocmd FileType typescript setlocal ts=4 sts=4 sw=4
autocmd FileType typescriptreact setlocal ts=4 sts=4 sw=4
autocmd FileType javascript setlocal ts=2 sts=2 sw=2
autocmd FileType php setlocal ts=4 sts=4 sw=4
autocmd FileType html setlocal ts=2 sts=2 sw=2
autocmd FileType vue setlocal ts=2 sts=2 sw=2
autocmd FileType go setlocal noet ts=4 sw=4 sts=4

au BufRead,BufNewFile *.ex,*.exs set filetype=elixir
au BufRead,BufNewFile *.eex,*.heex,*.leex,*.sface,*.lexs set filetype=eelixir
au BufRead,BufNewFile mix.lock set filetype=elixir
autocmd FileType elixir,eelixir nnoremap<buffer> <Leader>p :MixFormat<cr>

let g:mix_format_silent_errors = 1

if filereadable('/usr/bin/fzf')
  set rtp+=/usr/bin/fzf
else
  set rtp+=/usr/local/opt/fzf
endif

let g:go_fmt_command = "goimports"

nmap ; :Buffers<CR>
" nmap <Leader>t :Files<CR>

map \] :wa\|<Up><CR>

let g:vue_pre_processors = ['scss']
let test#strategy = "tslime"
let g:tslime_always_current_session = 1
let g:tslime_always_current_window = 1
let g:tslime_autoset_pane = 1

